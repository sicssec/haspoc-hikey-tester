#!/usr/bin/python
# SW description:
# This code test guests on HiKey board
# HW description:
# Beaglebone and CBB-relay board
# Power is connected to K1: 1CO, 1NC

import serial, time, os, sys
import subprocess
import atexit

test_scope = sys.argv[1] # std / ext

#
# helpers
#
def call(cmd):
    try:
        print("Executing ", cmd)
        subprocess.check_call( cmd.split(" ") )
    except Exception as e:
        raise Exception("Command failed", cmd, e)

#
# ports
#
def port_read(p):
    ret = ""
    while p.inWaiting() > 0:
       ret += p.read()
    return ret

def ports_cleanup():
    for p in ports:
        p.flush()
        p.close()

#
# gpio
#

def gpio(cmd):
    call( "/usr/bin/sara_gpio " + cmd)

def gpio_close():
    try:
        gpio("stop")
    except:
        print("gpio cleanup failed, ignoring error")

# Reboot target, capture logs and exit when all channels have completed boot
def boot_target():
    gpio("poweroff unklink")  # power off, Flash link removed
    time.sleep(3)
    gpio("poweron")


    print("Target running")
    log = ["","",""]
    while not((log[0].find("buildroot") >= 0) and (log[1].find("buildroot") >= 0) and (log[2].find("buildroot") >= 0)):
        for i in range(0, len(ports)):
            log[i] += port_read(ports[i])

    for l in log:
        print("BOOT LOG", l) # DEBUG


# Transmit commands, receive result through serial channels and write logs to file
def transceive_serial(guest_logname):

    ret = 0
    logs = [ open("guest_" + guest_logname + str(i) + ".log", "w")
                  for i in range(0, len(ports))]

    for p in ports:
        p.flushInput()

    while True:
        line = cmd_f.readline()[:-1] # remove \r
        print("Command: " + line)

        channel = int(line[0])	# Channel number
        if (channel > 2):			# 0,1 and 2 allowed
            print("Invalid channel number")
            ret = 3
            break

        t_r = line[1]        # Transmit or Receive
        cmd = line[3:]
        port = ports[channel]
        log = logs[channel]

	# Check t_r command
        if t_r == 'E':	# End of function
            ret = 0
            break
        elif t_r == 'T':			# Transmit
            port.write(cmd + "\n")
            log.write(cmd + "\n")
            time.sleep(1)
        elif t_r == 'R':			# Receive
            print("Buffersize: " + str(port.inWaiting()))
            linech = port_read(port)
            log.write(linech)
            print("--- Receive: " + str(channel)),
            if linech.find(cmd) >= 0:		# Is there a match to keyword ?
                print("FOUND ---")
            else:
                print("NOT FOUND ---")
                log.write("\n!!!Keyword '" + cmd + "' not found, fail\n")
                ret = 1
                break

        elif t_r == 'D':      		# Delay
            print("Delay: "+cmd+"s")
            for i in range(0, int(cmd)):
                print(str(i)+"s Buffersize\t0: "+str(ports[0].inWaiting())+"\t1: "+str(ports[1].inWaiting())+"\t2: "+str(ports[1].inWaiting()))
                time.sleep(1)

        else:
            print("Keyword syntax error, exiting function")
            ret = 2
            break


    # extract remaining data in ports and close the logs before returning
    for i in range(0, len(ports)):
        logs[i].write("(END)\n<-- ")
        logs[i].write(port_read(ports[i]))
        logs[i].flush()
        logs[i].close()

    return ret



# --- Start script ---

# Open command file
cmd_f = open('guest_keywords_'+test_scope+'.seq', 'r')

# This reflects summary of test, "" = pass
test_passed = ""

# Serial setup
portnames = ["/dev/ttyO2", "/dev/ttyO5", "/dev/ttyO5"]
ports = [ serial.Serial(name, baudrate=115200, timeout=10.0) for name in portnames]
atexit.register(ports_cleanup)

# Relay setup
gpio("start")
atexit.register( gpio_close)


# Redirect console to file
sys.stdout = open('guest_test.log','w')
print(time.strftime("%c %Z"))

# Sequence data in file interpret loop
while True:
    line = cmd_f.readline()
    cmd = line[3:-1]
    print(cmd)

    if cmd == 'ENDPROG':
        break

    elif cmd == 'REBOOT':
        boot_target()

    else:
        result = transceive_serial(cmd)
        if (result > 0):					# Test failed
            test_passed += cmd + " "
            print("Exit function")
            while True:						# Look for end of function
                line = cmd_f.readline()
                t_r = line[1]
                if t_r == 'E':
                    break
# Sequence program end

# Print result
if test_passed == "":
    print("Guest test pass")
else:
    print("\033[91mGuest test fail@ "+test_passed)	# Red color
    sys.exit(20)
