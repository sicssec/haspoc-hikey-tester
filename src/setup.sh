#!/bin/bash

#
# this files should run once on SARA to setup the device to work
# with the test scripts
#

set -e

# make sure we have the latest software first!
apt-get update
apt-get upgrade -y
apt-get install python-pexpect


# compile and install the setuid gpio bridge
gcc sara_gpio.c -o sara_gpio
mv sara_gpio /usr/bin/sara_gpio
chmod 4755 /usr/bin/sara_gpio  # setuid root!!!


# create new udev and give group access to fastboot
cat > /etc/udev/rules.d/99-sara.rules << EOF
# fastboot protocol on HiKey
SUBSYSTEM=="usb", ATTR{idVendor}=="18d1", ATTR{idProduct}=="d00d", MODE="0660", GROUP="dialout"
# adb protocol on HiKey
SUBSYSTEM=="usb", ATTR{idVendor}=="12d1", ATTR{idProduct}=="1057", MODE="0660", GROUP="dialout"
# rndis for HiKey
SUBSYSTEM=="usb", ATTR{idVendor}=="12d1", ATTR{idProduct}=="1050", MODE="0660", GROUP="dialout"

EOF


# make sure user is member of dialout:
usermod -aG dialout ubuntu
newgrp ubuntu
