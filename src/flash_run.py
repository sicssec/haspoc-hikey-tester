#!/usr/bin/python
# SW description:
# This code is flashing HiKey board
# After flashing, the target will run and debug prints are also saved in file
# ---
# HW description:
# Beaglebone and CBB-relay board
# Power is connected to K1: 1CO, 1NC
# Flash link is connected to K2: 2CO, 2NO


import serial
import time
import os, sys
import subprocess
import atexit
import pexpect

# secure-boot=sb, arm-trusted-firmware=atf
boot_var = sys.argv[1]

#
# execution
#
def call(cmd):
    try:
        print("Executing ", cmd)
        subprocess.check_call( cmd.split(" ") )
    except Exception as e:
        raise Exception("Command failed", cmd, e)

#
# gpio
#

def gpio(cmd):
    call( "/usr/bin/sara_gpio " + cmd)

def gpio_close():
    try:
        gpio("stop")
    except:
        print("gpio cleanup failed, ignoring error")

#
# operations
#

def flash_target():
    gpio("poweroff link");
    time.sleep(3)
    gpio("poweron")
    time.sleep(3)

    print("Start loading boot")
    if boot_var == "sb":
        call("python hisi-idt.py --img1 flashloader.bin")
    elif boot_var == "atf":
        call("python hisi-idt.py --img1 l-loader.bin")
    else:
        print "No valid boot variant defined"
        sys.exit(10)
    time.sleep(0.5)			# Delay needed
    print("Start loading fastboot")
#    call("fastboot flash ptable ptable-linux.img")
    if boot_var == "sb":
        call("fastboot flash fastboot hsbf.bin")
        call("fastboot flash boot bl1.bin")
    elif boot_var == "atf":
        call("fastboot flash fastboot fip.bin")
    print('Flash done')

def run_target():
    gpio("poweroff unlink")
    time.sleep(3)
    gpio("poweron")

    print("Target running")

def capture_log(port,fname):
    f = open(fname+'.log', 'w')
    f.write("!!! time_stamp: " + time.strftime("%c %Z\n"))
    line = ""
    while True:
        ch = port.read()
        if ch == "":		# Timeout
            print("\n!!! end_of_log")     # End of log
            f.write("\n!!! end_of_log\n")
            f.close()
    	    return
        elif ch == '\r':	# End of line
            print(line),
            f.write(line)
            line = ""
        else:			# Append latest character
            line += ch

# Save the buffered input and exit, does not wait for timeout
def save_log(port,fname):
    f = open(fname+'.log', 'w')
    f.write("!!! time_stamp: " + time.strftime("%c %Z\n"))
    line = ""
    while (port.inWaiting()) > 0:
        ch = port.read()
        if ch == '\r':        # End of line
            f.write(line)
            line = ""
        else:                   # Append latest character
            line += ch
    f.write("\n!!! end_of_log\n")
    f.close()

#
# main
#

# Hikey-Debug UART3 is connected to Beaglebone UART5 = ttyO5
# The timeout is used for termination of script and release of tty, we assume the target has ended execution or crashed
port0 = serial.Serial("/dev/ttyO5", baudrate=115200, timeout=12.0) # This timeout is adjusted to max boot-time with 50MB payload
port1 = serial.Serial("/dev/ttyO2", baudrate=115200, timeout=20.0)
port2 = serial.Serial("/dev/ttyO4", baudrate=115200, timeout=20.0)

# Relay setup
gpio("start")
atexit.register( gpio_close)

# Run script
flash_target()
run_target()


port0.flushInput()
port1.flushInput()
port2.flushInput()

capture_log(port0,'port0')
save_log(port1,'port1')
save_log(port2,'port2')
port0.close()
port1.close()
port2.close()
