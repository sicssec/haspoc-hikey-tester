#!/usr/bin/python
import os, sys,time
import fcntl, string
import subprocess
import pexpect

test_scope = sys.argv[1]	# std / ext
boot_var = sys.argv[2]		# atf / sb

#
# helpers
#
def call(cmd):
    try:
        subprocess.check_call(cmd.split(" "))
    except:
        raise Exception("Command failed", cmd)

#
# gpio
#

def gpio(cmd):
    call( "/usr/bin/sara_gpio " + cmd)

def gpio_close():
    try:
        gpio("stop")
    except:
        print("gpio cleanup failed, ignoring error")


#
# logs
#

def cleanup():
    files = os.listdir(".")
    for f in files:
        if f.endswith(".log"):
            os.remove(f)


def flash_and_run():
    p.sendline('python flash_run.py '+boot_var)
    print("Run remote flash script, flash HiKey")

    p.expect('Start loading fastboot')
    print("Start fastboot flashing")

    p.expect('Flash done')
    print("Restart HiKey in run mode")


# Flash is done, target will run and we are expecting debug prints
def boot_log_parse():
    f = open('boot_keywords.seq','r')     # Keywords we are looking for are stored in file
    while True:
        keyword = f.readline()
        keyword = keyword[:-1]  # Remove \r
        if keyword == '': break

	print("Searching for:'"+keyword+"' ..."),
	i = p.expect([keyword,'!!! end_of_log'])
	if i == 1:  # Not found (e_o_l without hit)
	    print(keyword+" not found")
	    print("Boot log parse fail")
	    sys.exit(1)
	print("keyword found")

    print("Boot log parse pass")

def guest_tests():
    print("Start guest tests")
    p.sendline('python guest_test.py '+test_scope)
    p.expect('ubuntu@arm:', timeout=60)     # Wait for script termination
    p.sendline('sync')
    print("Waiting for file sync")
    p.expect('ubuntu@arm:', timeout=600) # Filehandling on Beaglebone is slow


def guest_tests_inspect():
    with open ("guest_test.log", "r") as t:	# Read entire file into testlog string
	testlog=t.read()
    last_cr = testlog[:-1].rfind('\n')
    print (testlog[last_cr+1:])								# Print last line in log (=result)

def perf_test():
    perf_f = open('perf_cmds_'+test_scope+'.seq', 'r')
    perf_log = open('performance.log','w')
    f_read = perf_f.readline()[:-1]

    while f_read != 'END':
	f_keyword = perf_f.readline()[:-1]
	f_min = float(perf_f.readline())
	f_max = float(perf_f.readline())

	perf_log.write("Opening file '"+f_read+"'\n")
	with open (f_read, "r") as t:	# Read entire file into string
	    txt_target=t.read()
	pos_f = txt_target[:-1].rfind(f_keyword) # Find last match
	pos_f = pos_f + len(f_keyword)	# End of keyword

	while (txt_target[pos_f] < '0' or txt_target[pos_f] > '9'): # Step to the value
	    pos_f += 1

	pos_end = pos_f
	while ((txt_target[pos_end] >='0' and txt_target[pos_end] <='9') or txt_target[pos_end] == '.'):
	    pos_end += 1

	val = float(txt_target[pos_f:pos_end])

	if (val >= f_min) and (val <= f_max):
	    perf_log.write("Value '"+f_keyword+"' is within range: "+str(val)+" (Min: "+str(f_min)+" , Max: "+str(f_max)+')\n')
	else:
	    perf_log.write("Value '"+f_keyword+"' is out of range: "+str(val)+" (Min: "+str(f_min)+" , Max: "+str(f_max)+')\n')
	    print("Value '"+f_keyword+"' is out of range: "+str(val)+" (Min: "+str(f_min)+" , Max: "+str(f_max)+')')
	    return 1

	f_read = perf_f.readline()[:-1]

    perf_log.close()
    return 0


# Script starts here
#os.chdir( os.environ["HOME"] + "/autoflash")
os.system("ls -al");


# system sanity check to fail early
try:
    call("/usr/bin/sara_gpio hello")
except:
    msg = '''

    ***********************************************************************
    SARA is not configured for this operation.
    Run this command from the remote directory (may restart SARA):

    "sudo bash setup.sh"

    ***********************************************************************
    '''
    print(msg)
    sys.exit(20)


# remove old logs
cleanup()

# testlog is the new stdout
#sys.stdout = open( 'test.log','w')
os.environ['TZ']= 'UTC-01CEST'
time.tzset()


print("time_stamp_start: "+time.strftime("%c %Z"))
print("Test scope: "+test_scope)
print("boot_var: " + boot_var)

p = pexpect.spawn('bash')
p.logfile = sys.stdout # file('.txt','w')

flash_and_run()
boot_log_parse()

guest_tests()
print("Fetching log files")
guest_tests_inspect()


p_test = perf_test()
if p_test == 0:
	print("Performance test passed")
else:
	print("\033[91mPerformance test failed!")	# Red text



print("\033[0m")
print("time_stamp_end: "+time.strftime("%c %Z"))
