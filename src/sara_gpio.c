/*
 * setuid utility to allow access to relevant gpio pins when not root.
 *
 * This setup may seem like a bad design from 1992, before we had
 * udev scripts. But for various reasons this setuid proxy seems to be
 * the only solution that works reliably on BBB :(
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include "unistd.h"

#define BASE "/sys/class/gpio"

/*
 * write to gpio /sys files.
 *
 * there are better interfaces for gpio but this way we can easier trace
 * and debug operation from shell. It is also closer to the python
 * library it replaced.
 */
void writefile(char *filename, char *data)
{
  FILE *fp;
  fp = fopen(filename, "w");
  if(!fp) {
    fprintf(stderr, "Could not open %s\n", filename);
    perror("writefile() failed");
    exit(20);
  }

  fprintf(fp, "%s\n", data);
  fflush(fp);
  fclose(fp);
}


int main(int argc, char **argv)
{
  int i;
  char *cmd;

  if(argc == 0) {
    fprintf(stderr, "Usage: %s <hello|sleep|start|stop|poweron|link|unlink>\n",
            argv[0]);
    return 3;
  }

  setuid(0);
  if(getuid()) {
    fprintf(stderr, "This file should run setuid root...\n");
    return 20;
  }


  for(i = 1; i < argc; i++) {
    cmd = argv[i];
    if(!strcmp(cmd, "hello")) {
      printf("itsme\n");
    } else if(!strcmp(cmd, "start")) {
      writefile(BASE "/export", "65"); /* P8_18 */
      writefile(BASE "/export", "115"); /* P9_27 */
      writefile(BASE "/gpio65/direction", "out");
      writefile(BASE "/gpio115/direction", "out");
    } else if(!strcmp(cmd, "stop")) {
      writefile(BASE "/unexport", "65"); /* P8_18 */
      writefile(BASE "/unexport", "115"); /* P9_27 */
    } else if(!strcmp(cmd, "poweroff")) {
      writefile(BASE "/gpio65/value", "1");
    } else if(!strcmp(cmd, "poweron")) {
      writefile(BASE "/gpio65/value", "0");
    } else if(!strcmp(cmd, "link")) {
      writefile(BASE "/gpio115/value", "1");
    } else if(!strcmp(cmd, "unlink")) {
      writefile(BASE "/gpio115/value", "0");
    } else {
      fprintf(stderr, "Unknown command: %s\n", cmd);
      return 20;
    }
  }

  return 0;
}
