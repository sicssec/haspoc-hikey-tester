HASPOC HiKey tester
===================

This repository contains data for executing remote tests on a HiKey board.
The tests require access to the SARA platform configured for HASPOC in addition
to the hypervisor compiled for HiKey with Linux payload.


LICENSE
-------

::

   Copyright 2016 SICS
   Copyright 2016 T2Data

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

Files in this repository are distributed under the Apache 2.0 license,
see *LICENSE.txt*.


An exception to the above license is *src/external/hisi-idt.py* which has been
published by Linaro under the BSD license:

https://github.com/96boards-hikey/burn-boot/
